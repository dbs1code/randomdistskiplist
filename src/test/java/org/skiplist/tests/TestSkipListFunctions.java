package org.skiplist.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.skiplist.RandomlyDistributedSkipList;

/**
 * The Class TestSkipListFunctions to test functionality of the skipList
 */
public class TestSkipListFunctions {

  /**
   * Check insertions
   */
  @Test
  public void checkInsert(){
    int[] elements = {2, 10, 3, 15, 5, 1, 23, 99, -20, -50, -100, 100, 20, 10, 2};
    
    RandomlyDistributedSkipList skipList = new RandomlyDistributedSkipList(null);
    
    for (int i : elements){
      System.out.println("Adding element " + i);
      assertTrue("Unable to insert element " + i + " in the list", skipList.add(i));
    }
  }
  
  /**
   * Check search capability
   */
  @Test
  public void checkSearch(){
    int[] elements = {2, 10, 3, 15, 5, 1, 23, 99, -20, -50, -100, 100, 20, 10, 2, 23, 4, 999, 10000, 555, 44, -9999, -1, 6, 24, 16};
    int[] notPresentElements = {-666, -2, 25, 93, -21, 11, 998, 997, 553, 560};
    RandomlyDistributedSkipList skipList = new RandomlyDistributedSkipList(null);
    
    for (int i : elements){
      assertTrue("Unable to insert element " + i + " in the list", skipList.add(i));
    }
    
    for (int i : notPresentElements){
      assertFalse("Found absent element " + i + " in the list", skipList.contains(i));
    }
    
    // skipList.printListOnConsole();
    // skipList.printStats(); 
  }
  
  /**
   * Check if list is sorting correctly
   */
  @Test
  public void checkSorting(){
    Set<Integer> sortedSet = new TreeSet<Integer>();
    RandomlyDistributedSkipList skipList = new RandomlyDistributedSkipList(null);
    Random rnd = new Random();
    
    for (int i = 0; i < 10000; i++){
      int num = rnd.nextInt();
      sortedSet.add(num);
      skipList.add(num);
    }
    
    // convert sortedSet to array
    Integer[] primaryArray = sortedSet.toArray(new Integer[]{});
    Integer[] testArray = skipList.toArray();
    
    assertTrue("Some issue in sorting", Arrays.equals(primaryArray, testArray));
    // skipList.printListOnConsole();
    skipList.printStats(); 
  }
  
  /**
   * Corner cases to test null list, null insertions, null search
   */
  @Test
  public void cornerCases(){
    RandomlyDistributedSkipList skipList = new RandomlyDistributedSkipList(null);
    
    assertFalse(skipList.contains(100));
    assertFalse(skipList.add(null));
    assertTrue(skipList.add(100));
    assertTrue(skipList.add(200));
    assertFalse(skipList.contains(null));
    skipList.printListOnConsole();
    skipList.printStats(); 
    
  }
  
}
