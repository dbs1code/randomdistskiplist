package org.skiplist.interfaces;

/**
 * The Interface Promotable provides a way to plugin dynamically promotable logic
 */
public interface IPromotable {
  
  /**
   * Promote the element
   * 
   * The element o is kept generic, as implementor can decide on information needed from SkipList.
   * But, if promote logic does not depend on List's nature, then it can ignore it.
   *
   * @param o the o
   * @return true, if successful
   */
  public boolean promote(Object o);

}
