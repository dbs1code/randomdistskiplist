package org.skiplist.interfaces;

/**
 * The Interface ISkipList provides generic interface for SkipList implementation 
 *
 * @param <T> the generic type
 */
public interface ISkipList<T> {
  
  
  /**
   * Adds an element to the SkipList.
   *
   * @param e the element
   * @return true, if successful
   */
  public boolean add(T e);
  
  
  /**
   * Check if SkipList contains the element e.
   *
   * @param e the element
   * @return true, if successful
   */
  public boolean contains(T e);

}
