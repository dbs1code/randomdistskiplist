package org.skiplist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.skiplist.interfaces.ISkipList;
import org.skiplist.interfaces.IPromotable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.System.*;

/**
 * The Class RndDistSkipList implements a SkipList where levels are randomly distributed.
 */
public class RandomlyDistributedSkipList implements ISkipList<Integer> {
  
  /** The start node. */
  private Node<Integer> startNode;
  private long numElementsInList;
  private List<Integer> numElementsPerLevel = new ArrayList<Integer>();
  private IPromotable checkPromotion = null;
  Logger logger = LoggerFactory.getLogger(RandomlyDistributedSkipList.class);


  /**
   * Instantiates a new randomly distributed SkipList.
   */
  public RandomlyDistributedSkipList(IPromotable promotable){
    startNode = new Node<Integer>(null);
    startNode.getNextPointers().add(null);
    
    if (promotable == null){
      // use default implementation
      checkPromotion = new RandomlyDistributedPromotable();
    } else {
      checkPromotion = promotable;
    }
  }
  
 
  /**
   * Promote the element to the next level.
   *
   * @param o the o
   * @return true, if successful
   */
  private boolean promote(int level) {
    // TODO Auto-generated method stub
    return false;
  }
  
  private int compare(Integer nextVal, Integer element){
    int rslt = nextVal.compareTo(element);
    if (rslt > 0){
      rslt = 1;
    } else if (rslt < 0){
      rslt = -1;
    }
    return rslt;
  }
   
  /* (non-Javadoc)
   * @see org.skiplist.interfaces.ISkipList#add(java.lang.Object)
   */
  public boolean add(Integer e) {
    
    if (e == null){
      logger.error("Can not insert null elements in the list");
      return false;
    }
    
    // Always start from topmost level of leftmost column
    Node<Integer> currNodePtr = startNode;
    int currLevel = startNode.getNextPointers().size() - 1;
    List<Node<Integer>> tracePaths = new ArrayList<Node<Integer>>();
    
    logger.debug("Adding element " + e);
    while(true){
      
      // We always start from top-left-most node, which is empty
      // Under steady state, compare next value pointed by next node, and take a call
      // if one should move horizontally (e > val)
      //             or move vertically (e < val)
      //             or skip as number is already present
      Node<Integer> nextNodePtr = currNodePtr.getNextPointers().get(currLevel);
      
      if (nextNodePtr != null){
        // There is next node, so compare
        Integer nextVal = nextNodePtr.getValue();
        
        switch (compare(nextVal, e)){
        case 1:
          // move down
          if (currLevel > 0){
            logger.debug("Adding " + currNodePtr.getValue() + " to tracePath");
            tracePaths.add(currNodePtr);
          }
          currLevel--;
          break;
        case -1:
          // move horizontally, and compare with the next of that node
          // basically, move the pointer, and continue the loop
          currNodePtr = nextNodePtr;
          continue;
        case 0:
          logger.warn("Element " + e + " is already present in the list");
          return true;
        default:
          logger.error("Unable to compare element " + e);
          return false;
        }        
      }
      else {
        // there is no next node available, so keep moving down
        if (currLevel > 0){
          logger.debug("Adding " + currNodePtr.getValue() + " to tracePath");
          tracePaths.add(currNodePtr);
        }
        currLevel--;
      }
      
      // We have hit rockbottom, and thats the place to insert the node.
      if (currLevel < 0){
        // Its time to insert
        Node<Integer> newNode = new Node<Integer>(e);
        newNode.getNextPointers().add(currNodePtr.getNextPointers().get(0));
        currNodePtr.getNextPointers().set(0, newNode);
        numElementsInList++;
        if (numElementsPerLevel.isEmpty()){
          // first element of the list anyways
          numElementsPerLevel.add(1);
        } else {
          // increment by one
          numElementsPerLevel.set(0, numElementsPerLevel.get(0) + 1);
        }
        
        logger.debug ("Added " + e + " at level " + 0 + " linked with prevNode " + currNodePtr.getValue());
        
        // Try promotion now
        int i = 1;
        int tracePathsIdx = tracePaths.size() - 1;
        while (true){
          if (checkPromotion.promote((Object) i)){
            // promote to ith level
            logger.debug("promoting " + e + " to " + i + " th level");
            
            // check if there is any previous node available to connect to this node at new level
            // if not, then use startNode
            Node<Integer> prevLink = null;
            if (tracePathsIdx >= 0){
              prevLink = tracePaths.get(tracePathsIdx);
              logger.debug("Adding preLink as " + prevLink.getValue() + " at level " + i + " for node " + e);
              tracePathsIdx--;
            }
            else {
              // add null level at startNode
              int nStartNodeLvl = startNode.getNextPointers().size();
              logger.debug("Adding startNode as prevLink at level " + i );
              // startNode.getNextPointers().add(null);
              prevLink = startNode;
            }
            
            // This is applicable for use case, where element is promoted to the level, where 
            // no other element has reached.
            int nLevel = prevLink.getNextPointers().size() - 1;
            if (i > nLevel){
              // add null pointer to the prev path pointer
              prevLink.getNextPointers().add(null);
              logger.debug("Adding null link to prevLink (" + prevLink.getValue() 
                + "] promote level " + i + " prevLink levels" + nLevel);
            }
            newNode.getNextPointers().add(prevLink.getNextPointers().get(i));
            prevLink.getNextPointers().set(i, newNode);
            if (numElementsPerLevel.size() < (i + 1)){
              // add it
              // this list will always grow sequentially
              numElementsPerLevel.add(1);
            } else {
              // increment it
              numElementsPerLevel.set(i, numElementsPerLevel.get(i) + 1);
            }
            i++;
          } else {
            // stop promotion and exit the while
            break;
          }
        }
        // exit while
        break;
      }
    }
    return true;
  }

  /* (non-Javadoc)
   * @see org.skiplist.interfaces.ISkipList#contains(java.lang.Object)
   */
  public boolean contains(Integer e) {
    if (e == null){
      logger.error("Can not search null element");
      return false;
    }
    Node<Integer> ptr = startNode;
    int hops = 0;
    int currentLevel = startNode.getNextPointers().size() - 1;
    // always start with left-most top-most node
    while (currentLevel >= 0){
      Node<Integer> nextPtr = ptr.getNextPointers().get(currentLevel);
      if (nextPtr == null){
        currentLevel--;
        continue;
      }
      int compareRslt = compare(nextPtr.getValue(), e);
      hops++;
      switch(compareRslt){
      case 1:
        // number is less than the next number
        // so, go vertical
        currentLevel--;
        break;
      case -1:
        // number is higher than next number
        // so, go horizontal
        ptr = nextPtr;
        break;
      case 0:
        logger.info("SkipList Search: number " + e + " is present .. hops " + hops);
        return true;
      }
    }
    logger.info("SkipList Search: number " + e + " not present .. hops " + hops);
    return false;
  }

  
  /**
   * Search the element using sequential search and return number of hops
   * return +ve number of hops, if found.
   * return -ve number of hops, if not found
   *
   * @param e the e
   * @return the int
   */
  int search_N_countHops_Sequential_Search(Integer e){
    int hops = 0;
    if (e == null){
      return hops;
    }
    Node<Integer> ptr = startNode.getNextPointers().get(0);
    while (ptr != null){
      int compareRslt = compare(ptr.getValue(), e);
      hops++;
      switch(compareRslt){
      case 1:
        // number is less than the current number
        // so, number is not there in the list
        logger.info("Sequential Search: number " + e + " not present .. hops " + hops);
        return (hops * -1);
      case -1:
        // number is higher than current number
        // so, go to the next one
        ptr = ptr.getNextPointers().get(0);
        break;
      case 0:
        logger.info("Sequential Search: number " + e + " is present .. hops " + hops);
        return hops;
      }      
    }
    logger.info("Sequential Search: number " + e + " not present .. hops " + hops);
    return (hops * -1);
  }
  
  
  /**
   * Search the element using SkipList search and return number of hops
   * return +ve number of hops, if found.
   * return -ve number of hops, if not found
   * 
   * @param e the e
   * @return +ve integer if element found, else returns -ve integer 
   *         the integer denotes number of hops, regardless of sign
   */
  int search_N_countHops_SkipList_Search(Integer e) {
    int hops = 0;
    if (e == null){
      logger.error("Can not search null element");
      return hops;
    }
    Node<Integer> ptr = startNode;
    int currentLevel = startNode.getNextPointers().size() - 1;
    // always start with left-most top-most node
    while (currentLevel >= 0){
      Node<Integer> nextPtr = ptr.getNextPointers().get(currentLevel);
      if (nextPtr == null){
        currentLevel--;
        continue;
      }
      int compareRslt = compare(nextPtr.getValue(), e);
      hops++;
      switch(compareRslt){
      case 1:
        // number is less than the next number
        // so, go vertical
        currentLevel--;
        break;
      case -1:
        // number is higher than next number
        // so, go horizontal
        ptr = nextPtr;
        break;
      case 0:
        logger.info("SkipList Search: number " + e + " is present .. hops " + hops);
        return hops;
      }
    }
    logger.info("SkipList Search: number " + e + " not present .. hops " + hops);
    return (hops * -1);
  }

  
    
  /**
   * Checks if is present via sequential Search.
   *
   * @param n the n
   * @return true, if is present via sequential Search
   */
  public boolean isPresentViaSeqSrch(Integer e){
    int hops = 0;
    Node<Integer> ptr = startNode.getNextPointers().get(0);
    while (ptr != null){
      int compareRslt = compare(ptr.getValue(), e);
      hops++;
      switch(compareRslt){
      case 1:
        // number is less than the current number
        // so, number is not there in the list
        logger.info("Sequential Search: number " + e + " not present .. hops " + hops);
        return false;
      case -1:
        // number is higher than current number
        // so, go to the next one
        ptr = ptr.getNextPointers().get(0);
        break;
      case 0:
        logger.info("Sequential Search: number " + e + " is present .. hops " + hops);
        return true;
      }      
    }
    logger.info("Sequential Search: number " + e + " not present .. hops " + hops);
    return false;
  }
  
  public void printListOnConsole(){
    List<Node<Integer>> levels = startNode.getNextPointers();
    if (levels.isEmpty()){
      System.out.println("it is empty");
    }
    // Traverse vertically top-down
    System.out.println("Total levels are " + startNode.getNextPointers().size());
    for (int i = levels.size() - 1; i >= 0; i--){
      // traverse horizontally
      Node<Integer> n = levels.get(i);
      boolean firstNode = true;
      while (n != null){
        if (firstNode){
          System.out.print("Level [" + i + "] => ");
          firstNode = false;
        }
        System.out.print(n.getValue());
        int nLevel = n.getNextPointers().size();
        if (nLevel > i){
          n = n.getNextPointers().get(i);
          if (n != null){
            System.out.print("->");
          }
        } else {
          n = null;
        }
      }
      System.out.println("\n");
    }
  }
  
  /**
   * Prints the stats.
   */
  public void printStats(){
    out.println("Total number of elements in the list: " + numElementsInList);
    out.println("Total number of levels in the list: " + startNode.getNextPointers().size());
    for (int level = 0; level <= (numElementsPerLevel.size() - 1); level++){
      out.println("Total elements at level " + level + " are: " + numElementsPerLevel.get(level));
    }
  }
  
  public Integer[] toArray(){
    // we need to return array of bottom most level
    Node<Integer> levelZeroList = startNode.getNextPointers().get(0);
    List<Integer> toArrayList = new ArrayList<Integer>((int) numElementsInList);
    
    Node<Integer> currPtr = levelZeroList;
    while (currPtr != null){
      toArrayList.add(currPtr.getValue());
      currPtr = currPtr.getNextPointers().get(0);
    }
    
    return toArrayList.toArray(new Integer[]{});
  }
  

}
