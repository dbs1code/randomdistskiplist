package org.skiplist;

import java.util.Random;

import org.skiplist.interfaces.IPromotable;

/**
 * @author Dhananjay
 * 
 * The Class RandomlyDistributedPromotable.
 * 
 * It generates triggers based on whether random number is ith power of base  
 * 
 */
class RandomlyDistributedPromotable implements IPromotable {

  private Random randomizer = new Random();
  private Integer promoteModder = 4; 
  private Integer bound = -1;
  
  /**
   * Instantiates a new randomly distributed promotable.
   */
  public RandomlyDistributedPromotable(){
  }
  
  /**
   * Instantiates a new randomly distributed promotable.
   *
   * @param modder the modder
   * @param bound the bound
   */
  public RandomlyDistributedPromotable(int modder, int bound){
    promoteModder = modder;
    this.bound = bound;
  }
  
  /* (non-Javadoc)
   * @see org.skiplist.interfaces.IPromotable#promote(java.lang.Object)
   */
  public boolean promote(Object o) {
    Integer level = (Integer) o;
    
    // Check if random number is ith power of promoteModder where i is level
    if (bound > 0){
      return (randomizer.nextInt(bound) % (Math.pow(promoteModder, level)) == 0 ? true : false);
    } else {
      return (randomizer.nextInt() % (Math.pow(promoteModder, level)) == 0 ? true : false);
    }
  }

}
