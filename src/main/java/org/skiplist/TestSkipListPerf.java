package org.skiplist;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

/**
 * @author Dhananjay
 * special test case added sources to test performance using unexposed package scoped methods.
 */
public class TestSkipListPerf {

  /**
   * Test performance.
   */
  @Test
  public void testPerformance() {
    
    Map<String, RandomlyDistributedSkipList> skipLists = new HashMap<String, RandomlyDistributedSkipList>();
    int maxElements = 500000;
    int boundLmt = 5000000;
    
    // create skiplist of 100,000 elements
      skipLists.put("skipList_128_UB",new RandomlyDistributedSkipList(new RandomlyDistributedPromotable(128, -1)));
      skipLists.put("skipList_4_UB",new RandomlyDistributedSkipList(new RandomlyDistributedPromotable(4, -1)));
//    skipLists.put("skipList_8_UB", new RndDistSkipList(new RandomlyDistributedPromotable(8, -1)));
//    skipLists.put("skipList_16_UB", new RndDistSkipList(new RandomlyDistributedPromotable(16, -1)));
//    skipLists.put("skipList_2_B", new RndDistSkipList(new RandomlyDistributedPromotable(2, 10000)));
//    skipLists.put("skipList_4_B", new RndDistSkipList(new RandomlyDistributedPromotable(4, 10000)));
//    skipLists.put("skipList_8_B", new RndDistSkipList(new RandomlyDistributedPromotable(8, 10000)));
//    skipLists.put("skipList_16_B", new RndDistSkipList(new RandomlyDistributedPromotable(16, 10000)));
//    
    
    // Fill up list
    Random rnd = new Random();
    int randomSeed = rnd.nextInt();
    Set<String> listSet = skipLists.keySet();
    String[] listNames = listSet.toArray(new String[]{});
    Thread[] listThreads = new Thread[listNames.length];
    
    for (int i = 0; i < listNames.length; i++){
      listThreads[i] = new Thread(new InsertIntoList(skipLists.get(listNames[i]), new Random(randomSeed), maxElements, boundLmt), listNames[i]);
      listThreads[i].start();
    }
    
    // wait for threads to get over
    for(int i = 0; i < listThreads.length; i++){
      try {
        listThreads[i].join();
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    
    // print stats
    for (String lName : listNames){
      System.out.println("\nStats for " + lName + "  ==> ");
      skipLists.get(lName).printStats();
      // skipLists.get(lName).printListOnConsole();
    }
    
    // Compare the performance wrt sequential search for random numbers.
    // we will loop in till we get 5 hit and 5 absent results
    System.out.println("\n Comparing Sequential and SkipList based search now ...\n");
    rnd = new Random();
    int hits = 0, mishits = 0;
    int num;
    while (hits < 5 || mishits < 5){
      if (boundLmt > 0){
        num = rnd.nextInt(boundLmt);
      } else {
        num = rnd.nextInt();
      }
      for (int j = 0; j < listNames.length; j++){
        RandomlyDistributedSkipList currL = skipLists.get(listNames[j]);
        int hopsSS = currL.search_N_countHops_Sequential_Search(num);
        int hopsSL = currL.search_N_countHops_SkipList_Search(num);
        boolean isPrinted = false;
        // System.out.println("Checking for num " + num + " SS = " + hopsSS + " SL = " + hopsSL);
        if (hopsSS > 0){
          if (hits < 5){
            System.out.println(listNames[j] + " : Seq Srch Found " + num + " EXISTS in " + hopsSS + " hops");
            isPrinted = true;
          }
        } else if (hopsSS < 0){
          if (mishits < 5){
            isPrinted = true;
            System.out.println(listNames[j] + " : Seq Srch Found " + num + " DOESNT EXIST in " + (hopsSS * -1) + " hops");
          }
        } else if (hopsSS == 0){
          System.out.println(listNames[j] + " : Seq Srch ERROR during search for " + num );
        }
        
        if (hopsSL > 0){
          if (hits < 5){
            System.out.println(listNames[j] + " : SkipL Srch Found " + num + " EXISTS in " + hopsSL + " hops");
            isPrinted = true;
            if (j == (listNames.length - 1)){
              hits++;
            }
          }
        } else if (hopsSL < 0){
          if (mishits < 5){
            isPrinted = true;
            System.out.println(listNames[j] + " : SkipL Srch Found " + num + " DOESNT EXIST in " + (hopsSL * -1) + " hops");
            if (j == (listNames.length - 1)){
              mishits++;
            }
          }
        } else if (hopsSL == 0){
          System.out.println(listNames[j] + " : SkipL Srch ERROR during search for " + num );
        }
        if (isPrinted){
          System.out.println("\n");
        }
      }
      
    }
    
  }

}


/**
 * The Class InsertIntoList is useful for filling in large amount of data into the list in parallel
 */
class InsertIntoList implements Runnable{
  RandomlyDistributedSkipList thisList;
  Random rnd;
  Integer maxElements;
  Integer boundLmt;
  public InsertIntoList(RandomlyDistributedSkipList workingList, Random rnd, Integer maxElements, Integer boundLmt){
    this.thisList = workingList;
    this.rnd = rnd;
    this.maxElements = maxElements;
    this.boundLmt = boundLmt;
  }
   public void run(){
     System.out.println("Filling in " + maxElements + " elements for " + Thread.currentThread().getName());
     int ret = 0;
     int randomNo;
     for (int i = 0; i < maxElements; i++){
       if (boundLmt != null && boundLmt > 0){
         randomNo = rnd.nextInt(boundLmt);
       } else {
         randomNo = rnd.nextInt();
       }
       thisList.add(randomNo);
     }
     System.out.println("Finished filling up " + maxElements + " elements into " + Thread.currentThread().getName());
   }
}