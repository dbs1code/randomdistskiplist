package org.skiplist;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dhananjay
 * 
 * The Class Node represent a node in SkipList
 *
 * @param <T> the generic type
 */
class Node<T> {
  
  /** The value. */
  private T value;
  

  /** The next pointers. */
  private List<Node<T>> nextPointers;
  
  /**
   * Instantiates a new node.
   *
   * @param e the e
   */
  public Node(T e){
    value = e;
    nextPointers = new ArrayList<Node<T>>(5);
  }
  
  /**
   * Gets the value.
   *
   * @return the value
   */
  public T getValue() {
    return value;
  }

  /**
   * Gets the next pointers.
   *
   * @return the next pointers
   */
  public List<Node<T>> getNextPointers() {
    return nextPointers;
  }

}
